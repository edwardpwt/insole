from __future__ import (absolute_import, division, print_function, unicode_literals)
from flask import Flask, render_template, request, jsonify
import json
#import plotly
#import plotly.graph_objs as go
from datetime import datetime
from bluepy.btle import *
from collections import deque, defaultdict
import threading
import struct
import time
import logging

import paho.mqtt.publish as publish


# import pymongo
# from pymongo import MongoClient
# import datetime
# import urllib
# import pprint
# import sys
# from collections import deque
# pyVersion=sys.version[0]

connections = [None,None]
connection_threads = []
scanner = Scanner(0)


bt_addrs = ["98:7b:f3:58:37:0d","98:7b:f3:5a:d2:3c"]
#bt_addrs = ["88:c2:55:ac:32:d6","98:7b:f3:5a:d2:3c"]
#small,big


# if pyVersion !="3":
#     mongo_url = "mongodb://root:"+urllib.quote_plus("FiF@b0nu$")+"@ds111562.mlab.com:11562/insole"
# else:
#     mongo_url = "mongodb://root:"+urllib.parse.quote_plus("FiF@b0nu$")+"@ds111562.mlab.com:11562/insole"


# data_q = deque(maxlen=900)

# def getMAC(interface='eth0'):
#   # Return the MAC address of the specified interface
#   try:
#     str = open('/sys/class/net/%s/address' %interface).read()
#   except:
#     str = "00:00:00:00:00:00"
#   return str[0:17]

# rpi_addr = getMAC()

class NotificationDelegate(DefaultDelegate):
    
    def __init__(self,connection_index):
        DefaultDelegate.__init__(self)
        self.connection_index = connection_index
    def handleNotification(self, cHandle, data):
        if self.connection_index == 0:
            result = struct.unpack("<BBBBBBBBBB",str(data))
           # result = struct.unpack("<BBBBBBBBBBBBB", str(data))
            publish.single("Left",str(result),hostname="192.168.11.14",port=1883)
        else:
            result = struct.unpack("<BBBBBBBBBB", str(data))
            publish.single("Right",str(result),hostname="192.168.11.14",port=1883)


        # time = datetime.datetime.now()
        # datum = (time,result)
        # data_q.append(datum)
        print (self.connection_index,result)                                         


class ConnectionHandlerThread(threading.Thread):
    
    def __init__(self, connection_index):
        threading.Thread.__init__(self)
        self.connection_index = connection_index
    def run(self):
        connection = connections[self.connection_index]
        servUUID = UUID("A495FF20-C5B1-4B44-B512-1370F02D74DE")
        dataUUID = UUID("A495FF21-C5B1-4B44-B512-1370F02D74DE")
        try:
            connection.withDelegate(NotificationDelegate(self.connection_index))
            service = connection.getServiceByUUID(servUUID)
            ch = service.getCharacteristics(dataUUID)[0]

            # Enable notification
            connection.writeCharacteristic(ch.getHandle()+1, bytearray.fromhex('0100') )
            time.sleep(0.5)
            while True:
                if connection.waitForNotifications(2.0):
                    continue # Else end thread
                print ("Waiting...")
        except BTLEException as error:
            print ("2."+str(error))

class ScannerThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        while True:
            print('Scaning')
            devices = scanner.scan(2)
            for d in devices:
            # print d.addr
             if d.addr in bt_addrs:
               print('connected')  
               connection_index = 0 if d.addr==bt_addrs[0] else 1
               try:
                p = Peripheral(d, "random")
                connections[connection_index]=p
                t = ConnectionHandlerThread(connection_index)
                t.start()
                connection_threads.append(t)
               except BTLEException as error:
                print ("1."+str(error))

class MongoDBThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        while True:
          if len(data_q)==900:
            print("Insert Data to Mongo")
            client = MongoClient(mongo_url)
            db = client.insole
            if ("sensor" not in db.collection_names(include_system_collections=False)):
                collection = db.sensor#declare a collection named dummy sensor
                post = {"uploadTime":datetime.datetime.now(),
                        "sensorMacAddr":bt_addrs,
                        "gatewayMacAddr":rpi_addr,
                        "time":[datum[0]for datum in data_q],
                        "x":[datum[1][0]+datum[1][1]*256 for datum in data_q],
                        "y":[datum[1][2]+datum[1][3]*256 for datum in data_q],
                        "z":[datum[1][4]+datum[1][5]*256 for datum in data_q],
                        "fsr1":[datum[1][6]+datum[1][7]*256 for datum in data_q],
                        "fsr2":[datum[1][8]+datum[1][9]*256 for datum in data_q],
                }
                collection.insert_one(post)
            else:
                collection = db.sensor
                post = {"uploadTime":datetime.datetime.now(),
                        "sensorMacAddr":bt_addrs,
                        "gatewayMacAddr":rpi_addr,
                        "time":[datum[0]for datum in data_q],
                        "x":[datum[1][0]+datum[1][1]*256 for datum in data_q],
                        "y":[datum[1][2]+datum[1][3]*256 for datum in data_q],
                        "z":[datum[1][4]+datum[1][5]*256 for datum in data_q],
                        "fsr1":[datum[1][6]+datum[1][7]*256 for datum in data_q],
                        "fsr2":[datum[1][8]+datum[1][9]*256 for datum in data_q],
                }
                collection.insert_one(post)
            client.close()
            time.sleep(900)
          else:
            time.sleep(900)

def main():
    a = ScannerThread("ScannerThread")
    #b = MongoDBThread("MongoDBThread")
    a.start()
    #b.start()
if __name__ == "__main__":
    main()

